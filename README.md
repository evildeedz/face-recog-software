# Facial Recognition Sample

This is an open-source hobby project using facial_recognition & pyqt5 library.

The CNN part is commented out for now. The facial recognition part works perfectly (line 1291 onwards). Incase you want to check the CNN progress so far just uncommoment the lines which will add a newtab. 

---

## Instructions

1) Upload pictures of the person to train the model along with the name.
2) Click on train the moel.
3) After it is trained, you can either add more pictures, or use webcam and see the model draw a box around your face with the name of that person.
